<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RoomsController extends Controller
{
    /**
     * @Route("/rooms", name="rooms")
     */
    public function index()
    {
        return $this->render('rooms/index.html.twig', [
            'controller_name' => 'RoomsController',
        ]);
    }
}
